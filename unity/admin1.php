<?php
session_start();
if(!$_SESSION["aname"])
{
	header("Location:admin.php");
  exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="main.css"/>
<title>Admin Panel</title>
</head>

<body>
<header style="height:100px;"><h1>Admin Panel</h1>
<a href="adout.php"><h1 style="text-align:right;color:blue;">Logout</h1></a>
</header>
<br />
<br />
<br />
<br />
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if($_POST["lang"]=="Language")
	{
		echo "Select the language";
	}
	else
	{
	if($_FILES["fileToUpload"]["error"] == 4) {

	$target_file="upimage/noimage.jpg";

	}
	else
	{
		
		$target_dir = "upimage/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
$flag=0;

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
	
	if($imageFileType=="mp4" || $imageFileType=="avi" )
	{
		$flag=1;
	}
	else
	{
    echo "Sorry, file is not supported.";
	$target_file="upimage/noimage.jpg";
    $uploadOk = 0;
	}
}
if($flag==0){
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
	$target_file="upimage/noimage.jpg";
    $uploadOk = 0;
}

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
		$target_file="upimage/noimage.jpg";
    }
}

}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
$target_file="upimage/noimage.jpg";
// if everything is ok, try to upload file
} else {
	$target_file=$target_dir. date("Y-m-d h-i-sa").".".$imageFileType;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
        echo "Sorry, there was an error uploading your file.";
		$target_file="upimage/noimage.jpg";
    }

}
	}
   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$output="";
if($_POST["lang"]=="English")
{
$JAVA_HOME = "C:\wamp64\www\unity\Java\jdk1.8.0_121";
			
		$PATH = "$JAVA_HOME/bin";
        putenv("JAVA_HOME=$JAVA_HOME");
        putenv("PATH=$PATH");
 
       
		
		$smry = preg_replace("/[\r\n]*/","",$_POST["message"]);
		
		$output = shell_exec('java -classpath sum/bin improved_summary "'.$smry.'" 2>&1');
		
}

$dte=date("m/d/Y");

$stmt = $conn->prepare("INSERT INTO upload1 (ipath,message,title,flag,upload_date,smry) VALUES (?,?,?,?,?,?)");

$stmt->bind_param("sssiss", $target_file,$_POST["message"],$_POST["title"],$flag,$dte,$output);
$uid="";
if($stmt->execute()==TRUE)
{
echo "Successful";	

$stmt->close();
$sql="select ID from upload1 where title='".$_POST["title"]."'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $uid=$row["ID"]  ;
    }
} 

		$ary=explode(" ",$_POST["message"]);
		
		$l=count($ary);
	for($i=0;$i<$l;$i++)
	{
		$c=0;
		for($j=0;$j<$l;$j++)
		{
			if(strcmp($ary[$i],$ary[$j])==0)
			{
				$c++;
			}
			
		}
		$sql="insert into summary (content,count,uid) values('".$ary[$i]."',".$c.",".$uid.")";
		$conn->query($sql);
	}
	}
else
{
echo "Error: ".$conn->error;
}
$conn->close();

	}
}

?>


<h2 style="text-align:center;">New Upload</h2>
<h2></h2>
<form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  enctype="multipart/form-data">
Enter the title:<input type="text" name="title" />
Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    
    <select name="lang">
  <option value="Language">Language</option>
  <option value="English">English</option>
  <option value="Others">Others</option>
</select>
    
    <textarea name="message" rows="10" cols="30">
    
</textarea>
    <input type="submit" value="Upload file" name="submit">
</form>
<br />
<hr />
<div id="up" style="width:100%;height:1000px;overflow-y:scroll;">
<h2>Donot update in other language .Otherwise delete and reupload</h2>
<br />

<?php
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql="select * from upload1 order by ID desc";
$result = $conn->query($sql);
while($row = $result->fetch_assoc())
	{
		echo "<form id='form1' name='form1' method='post' action='update.php?id=".$row["ID"]."&&flag=".$row["flag"]."'  enctype='multipart/form-data'>
		Upload date:".$row["upload_date"]."&nbsp;
		Title:<input type='text' name='title' value='".$row["title"]."'/>
		File Location:<input type='text' name='efile' value='".$row["ipath"]."'/>Choose new file<input type='file' name='fileToUpload' id='fileToUpload'>
		Content:<textarea name='message' rows='10' cols='30'>".$row["message"]."</textarea>
		
		<input type='submit' value='Update' name='submit'>
				<a style='color:red;' onClick=\"javascript: return confirm('Please confirm deletion');\" href='delete.php?id=".$row["ID"]."'>Delete</a>

		</form>
		<hr>
		<br>";
	}

?>

</div>

</body>
</html>