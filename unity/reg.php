

<?php
session_start();
$name=$phno=$prfsn=$day=$month=$year=$state=$gender=$pwd1=$pwd2=$email="";
$ername=$erphno=$erprof=$erdob=$erstate=$ergender=$eruname=$erpwd1=$erpwd2=$eremail=$erfile="";
$cyr=date("Y");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$flag=0;
   if (empty($_POST["name"])) {
     $ername = "Name is required";
	 $flag=1;
   } else {
     $name = test_input($_POST["name"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
		 $flag=1;
       $ername = "Only letters and white space allowed"; 
     }
   }
   
   
   
   if (empty($_POST["email"])) {
     $eremail = "Email is required";
	 $flag=1;
   } else {
     $email = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $eremail = "Invalid email format";
	   $flag=1; 
     }
   
   
   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT email FROM member_info";
$result = $conn->query($sql);
   
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if(strcmp($row["email"],$email)==0)
		{
			$flag=1;
			$eremail="email already exist";
			break;
		}
    }
}
$conn->close();
   }
   
   
    if (empty($_POST["gender"])) {
     $ergender = "Gender is required";
	 $flag=1;
   } else {
     $gender = test_input($_POST["gender"]);
   }
   
   if (empty($_POST["pswd"])) {
     $erpwd1 = "Password is required";
	 $flag=1;
   } else {
     $pwd1 = test_input($_POST["pswd"]);
   }
   
   if (empty($_POST["rpswd"])) {
     $erpwd2 = "Retyping of password is required";
	 $flag=1;
   } else {
     $pwd2 = test_input($_POST["rpswd"]);
   }
   
   if(strcmp($pwd1,$pwd2)!=0)
   {
	   $erpwd2="Mismatch of password";
	   $flag=1;
   }
   
   
   
   if($flag==0)
   {
	   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind
$stmt = $conn->prepare("INSERT INTO member_info (Name,email,Gender,Password) VALUES (?,?,?,?)");
$stmt->bind_param("ssss", $name,$email,$gender,$pwd1);

$stmt->execute();
	   
	   $stmt->close();


	   
   echo "Successfull";
   $_SESSION["email"]=$email;
			$_SESSION["name"]=$name;
			header("Location:index.php");
  exit();
   }
   
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body style="background-color: #f1f1f1;">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php">G.U Times</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Registration</a></li>
        
      </ul>
      
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left" style="background-color:white;"> 
      <form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  enctype="multipart/form-data">
                      <h2>	Name:</h2>
                        <input type="text" name="name" id="name" style="width:100%;"/><span class="error" >*<?php echo $ername ?></span>
                      
  
                      <h2>E mail: </h2>
                        <label for="email"></label>
                        <input type="email" name="email" id="email" style="width:100%;"/>
                 <span class="error" >*<?php echo $eremail ?></span>
                      
                      <h2>Gender:
                        <label for="gender"></label>
                      </h2>
                      <p>
                        <label>
                          <input type="radio" name="gender"  <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male" style="width:100%;" />Male</label>
                        <label>
                          <input type="radio" name="gender"  <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female" style="width:100%;"/>Female</label>
                          <span class="error" >*<?php echo $ergender; ?></span>
                      </p>
                      <p>
                        <label for="uname"></label>
                      </p>
                      <h2>Password:</h2>
                        <label for="pswd"></label>
                        <input type="password" name="pswd" maxlength=10 style="width:100%;"/>
                        <span class="error" >*<?php echo $erpwd1 ;?></span>
                      
                      <h2>Re-Type Password:</h2>
                        <label for="rpswd"></label>
                        <input type="password" name="rpswd"  maxlength=10 style="width:100%;"/>
                        <span class="error" >*<?php echo $erpwd2; ?></span>
                      
                      <input type="submit" value="submit"/>
</form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>© 2017 GAUHATI UNIVERSITY ALL RIGHTS RESERVED</p>
</footer>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>
