<?php
session_start();
if(!$_SESSION["aname"])
{
	header("Location:admin.php");
  exit();
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if($_POST["lang"]=="Language")
	{
		echo "Select the language";
	}
	else
	{
	if($_FILES["fileToUpload"]["error"] == 4) {

	$target_file="upimage/noimage.jpg";

	}
	else
	{
		
		$target_dir = "upimage/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
$flag=0;

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
	
	if($imageFileType=="mp4" || $imageFileType=="avi" )
	{
		$flag=1;
	}
	else
	{
    echo "Sorry, file is not supported.";
	$target_file="upimage/noimage.jpg";
    $uploadOk = 0;
	}
}
if($flag==0){
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
	$target_file="upimage/noimage.jpg";
    $uploadOk = 0;
}

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
		$target_file="upimage/noimage.jpg";
    }
}

}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
$target_file="upimage/noimage.jpg";
// if everything is ok, try to upload file
} else {
	$target_file=$target_dir. date("Y-m-d h-i-sa").".".$imageFileType;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
        echo "Sorry, there was an error uploading your file.";
		$target_file="upimage/noimage.jpg";
    }

}
	}
   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
$output="";
if($_POST["lang"]=="English")
{
$JAVA_HOME = "C:\wamp64\www\unity\Java\jdk1.8.0_121";
			
		$PATH = "$JAVA_HOME/bin";
        putenv("JAVA_HOME=$JAVA_HOME");
        putenv("PATH=$PATH");
 
       
		
		$smry = preg_replace("/[\r\n]*/","",$_POST["message"]);
		
		$output = shell_exec('java -classpath sum/bin improved_summary "'.$smry.'" 2>&1');
		echo $output;
}

$dte=date("m/d/Y");

$stmt = $conn->prepare("INSERT INTO upload1 (ipath,message,title,flag,upload_date,smry) VALUES (?,?,?,?,?,?)");

$stmt->bind_param("sssiss", $target_file,$_POST["message"],$_POST["title"],$flag,$dte,$output);
$uid="";
if($stmt->execute()==TRUE)
{
echo "Successful";	

$stmt->close();
$sql="select ID from upload1 where title='".$_POST["title"]."'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $uid=$row["ID"]  ;
    }
} 

		$ary=explode(" ",$_POST["message"]);
		
		$l=count($ary);
	for($i=0;$i<$l;$i++)
	{
		$c=0;
		for($j=0;$j<$l;$j++)
		{
			if(strcmp($ary[$i],$ary[$j])==0)
			{
				$c++;
			}
			
		}
		$sql="insert into summary (content,count,uid) values('".$ary[$i]."',".$c.",".$uid.")";
		$conn->query($sql);
	}
	}
else
{
echo "Error: ".$conn->error;
}
$conn->close();

	}
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-3 sidenav">
     <form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  enctype="multipart/form-data">
Enter the title:<input type="text" name="title" /><hr />
Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <hr />
    <select name="lang">
  <option value="Language">Language</option>
  <option value="English">English</option>
  <option value="Others">Others</option>
</select>
    <hr />
    <textarea name="message" rows="10" cols="30">
    
</textarea>
<hr />
    <input type="submit" value="Upload file" name="submit">
</form>
    </div>
    <div class="col-sm-9 text-left"> 
      <?php
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql="select * from upload1 order by ID desc";
$result = $conn->query($sql);
while($row = $result->fetch_assoc())
	{
		echo "<form id='form1' name='form1' method='post' action='update.php?id=".$row["ID"]."&&flag=".$row["flag"]."'  enctype='multipart/form-data' style='margin-left:50px;'>
		Title:<input type='text' name='title' value='".$row["title"]."'/>Content:<textarea name='message' rows='10' cols='30'>".$row["message"]."</textarea>
		<br>File Location:<input type='text' name='efile' value='".$row["ipath"]."'/>Choose new file<input type='file' name='fileToUpload' id='fileToUpload'>
		
		
		<input type='submit' value='Update' name='submit'>
				<a onClick=\"javascript: return confirm('Please confirm deletion');\" href='delete.php?id=".$row["ID"]."'>Delete</a>

		</form>
		<hr>
		<br>";
	}

?>

    </div>
    
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>
