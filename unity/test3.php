<?php
 $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "Unity";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql="select * from upload1";
$result = $conn->query($sql);
$rows=$result->num_rows;
$page_rows=2;
$last=ceil($rows/$page_rows);
if($last<1)
{
	$last=1;
}
$pagenum=1;
if(isset($_GET['page']))
{
	$pagenum=preg_replace('#[^0-9]#','',$_GET['page']);
}
if($pagenum<1)
{
	$pagenum=1;
}
else if($pagenum>$last)
{
	$pagenum=$last;
}
$limit='LIMIT '.($pagenum-1)*$page_rows.','.$page_rows;
$sql='select * from upload1 order by ID desc '.$limit;
$result = $conn->query($sql);
$text1="Testimonials(<b>$rows</b>)";
$text2="Page <b>$pagenum</b>of<b>$last</b>";
$pagectrls="";
if($last!=1)
{
	if($pagenum>1)
	{
		$previous=$pagenum-1;
		$pagectrls .='<a href="'.$_SERVER['PHP_SELF'].'?page='.$previous.'">previous</a> &nbsp;&nbsp;';
		for($i=$pagenum-4;$i<$pagenum;$i++)
		{
			if($i>0)
			{
				$pagectrls .='<a href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</a> &nbsp;';
			}
		}
	}
	$pagectrls .=''.$pagenum.'&nbsp;';
	
	for($i=$pagenum+1;$i<=$last;$i++)
	{
		$pagectrls .='<a href="'.$_SERVER['PHP_SELF'].'?page='.$i.'">'.$i.'</a> &nbsp;';
		if($i >= $pagenum+4)
		{
			break;
		}
	}
	if($pagenum!=$last)
	{
		$next=$pagenum+1;
		$pagectrls .='<a href="'.$_SERVER['PHP_SELF'].'?page='.$next.'">next</a>';
	}
	$list='';
	while($row = $result->fetch_assoc())
	{
		$id=$row["ID"];
		$path=$row["ipath"];
		$message=$row["message"];
		$title=$row["title"];
		$flag=$row["flag"];
		if($flag==1)
		{
		
			$list .='<p><div class="div1" style="height:400px;width:400px;border:2px solid black;"><a href="chat.php?title='.$title.'"><h1>'.$title.'</h1></a> <video width="400px" height="300px" controls> <source src="'.$path.'" type="video/mp4"></video></div></p>';
	
		
		}
		else
		{
			
			$list .='<p><div class="div1" style="height:400px;width:400px;border:2px solid black;"><a href="chat.php?title='.$title.'"><h1>'.$title.'</h1></a>  <img src="'.$path.'" style="width:400px;height:200px;border:0"></div></p>';
		}
	}
	
}

$stmt1="select title from upload1";
$result = $conn->query($stmt1);
$i=0;
$table="";
while($row = $result->fetch_assoc())
{
	$table=str_replace(' ','',$row["title"]);
	$stmt2="select * from ".$table;
	$result1 = $conn->query($stmt2);
	if($result1->num_rows>=1)
	{
	$ary1[$i] =$result1->num_rows ;
	
	$ary2[$ary1[$i]]=$row['title'];
	$i++;
	}
}
rsort($ary1);
$txt="";
for($j=0;$j<$i;$j++)
{
	$txt .='<p><h3><a href="chat.php?title='.$ary2[$ary1[$j]].'">'.$ary2[$ary1[$j]].'</a></h3></p>';
}
//$ary1[]=NULL;
//$ary2[]=NULL;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
#page_ctrls>a {color:#06F;}
#page_ctrls>a:visited {color:#39C;}
a:link {
    text-decoration: none;
}
a:link {
    color: black;
}

/* visited link */
a:visited {
    color: black;
}
</style>
</head>

<body>
<div id="container" style="width:800px;height:800px;border:2px solid black;float:right;">
<h2><?php echo $text1; ?> Paged</h2>
<p><?php echo $text2 ; ?></p>
<p><?php echo $list ; ?></p>
<div id="page_ctrls"><?php echo $pagectrls ; ?></div>
</div>
<div id="trend" style="width:300px;height:800px;border:2px solid black;text-align:center;">
<h2>Trendings</h2><hr />
<div id="details" style="width:100%;height:100%;">
<?php echo $txt; $txt=NULL; ?>
</div>
</div>
</body>
</html>