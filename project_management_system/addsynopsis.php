<?php
session_start();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>


</head>

</head>

<body>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "report";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
	
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$ext = substr($target_file, 0,strpos($target_file, '.'));
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "pdf" && $imageFileType != "ppt" && $imageFileType != "docx"
&& $imageFileType != "txt" ) {
    echo "Sorry, only PDF, PPT, TXT & DOCS files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		 $table = substr($_SESSION["email"], 0,strpos($_SESSION["email"], '@'));
		$stmt="insert into stuploads(uploader,path,guide,result,email,domain,title) values(?,?,?,?,?,?,?)";
		$stmt->bind_param("sssssss",$_SESSION["name"],$target_file,'Pending','Pending',$_SESSION["email"],$_POST["domain"],$_POST["domain"]);
$stmt->execute();
$stmt->close();

		$conn->close();
		
		
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
}
?>


<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
    SELECT FILE TO UPLOAD:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload SYNOPSIS" name="submit"><br />
    ENTER THE TITTLE:<input type="text" value="TITLE" name="title"/><br />
    ENTER THE DOMAIN NAME:<input type="text" value="domain" name="domain" /><br />
</form>

</body>
</html>