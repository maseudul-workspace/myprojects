<!DOCTYPE html>
<html lang="en">
<head>
  <title>Verification</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body style="background-color: #f1f1f1;">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Project Manager</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Account Verification</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="adout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
      
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left" style="background-color:white;"> 
      <table class="table table-striped">
    <thead>
      <tr>
        <th>Student name</th>
        <th>Roll No</th>
        <th>Branch</th>
        <th>Semester</th>
        <th>Verify</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "report";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql="Select * from registration where verify='no'";
$result=$conn->query($sql);
 if ($result->num_rows > 0) {
	 while($row = $result->fetch_assoc())
	{
		echo "<tr>
        <td>".$row["name"]."</td>
        <td>".$row["rollno"]."</td>
        <td>".$row["branch"]."</td>
		<td>".$row["semester"]."</td>
		<td><a onClick=\"javascript: return confirm('Are your sure');\" href='confirm.php?id=1&&email=".$row["email"]."'>Yes</a>&nbsp;&nbsp;<a onClick=\"javascript: return confirm('Are your sure');\" href='confirm.php?id=2&&email=".$row["email"]."'>No</a></td>
      </tr>";
	}
 }
    ?>
      
      
      
    </tbody>
  </table>
     
      
      
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>

<footer class="container-fluid text-center" style="height:100px;">
  <p>© 2017 GAUHATI UNIVERSITY INSTITUTE OF SCIENCE AND TECHNOLOGY ALL RIGHTS RESERVED</p>
</footer>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>
