<?php
session_start();
$txt1="";
$txt2="";
$txt3="";

$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "report";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	if (!empty($_POST['upload']))
{
	$target_dir = "finalupload/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$ext = substr($target_file, 0,strpos($target_file, '.'));
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "pdf" && $imageFileType != "ppt" && $imageFileType != "docx"
&& $imageFileType != "txt" ) {
    echo "Sorry, only PDF, PPT, TXT & DOCS files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
	 $path=$target_dir. date("Y-m-d h-i-sa").".".$imageFileType;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$path)) {
        
		 
		$stmt=$conn->prepare("insert into final_report(path,email) values(?,?)");
		$stmt->bind_param("ss",$path,$_SESSION["email"]);
		
$stmt->execute();
$stmt->close();

		
		
		
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
	
}

if (!empty($_POST['imgload']))
{
	
	$target_dir = "finalimages/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$ext = substr($target_file, 0,strpos($target_file, '.'));
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png"
&& $imageFileType != "gif" ) {
    echo "Sorry, only jpeg & png files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
	 $path=$target_dir. date("Y-m-d h-i-sa").".".$imageFileType;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$path)) {
        
		 
		$stmt=$conn->prepare("insert into images(path,email) values(?,?)");
		$stmt->bind_param("ss",$path,$_SESSION["email"]);
		
$stmt->execute();
$stmt->close();

	
		
		
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
	
}


}

$sql="select * from final_report where email='".$_SESSION["email"]."'";

$result = $conn->query($sql);

if ($result->num_rows > 0)
{
	$txt1="hidden='true'";
}
else
{
	$txt2="hidden='true'";
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Final Report</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 585px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
	
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Project Manager</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Final Report</a></li>
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content" style="overflow-y:scroll;">
    <div class="col-sm-3 sidenav">
      
      <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data" <?php echo $txt1; ?> style="width:100%;">

<div class="form-group">
      <label for="fileupload">Upload Final Report</label>
      <input type="file" name="fileToUpload" id="fileToUpload">
    </div>
    <input type="submit" value="Upload" name="upload">
    </form>
      <div id="delete" <?php echo $txt2; ?> > Your final report has been uploaded.Delete before reuploading<br> <?php

echo "<a onClick=\"javascript: return confirm('Please confirm deletion');\" href='findel.php'>Delete Synopsis</a>"; 


?></div>

<br>
<br>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data"  style="width:100%;">

<div class="form-group">
      <label for="fileupload">Upload Project Images</label>
      <input type="file" name="fileToUpload" id="fileToUpload">
    </div>
    <input type="submit" value="Upload" name="imgload">
    </form>
      
    </div>
    <div class="col-sm-9 text-left" > 
      <div class="row">
  <?php  
    $sql="select * from images where email='".$_SESSION["email"]."'";
$result = $conn->query($sql);
if ($result->num_rows > 0)
{
	while($row = $result->fetch_assoc())
	{
		echo "<div class='col-md-4'>
      <div class='thumbnail'>
        <a href='".$row["path"]."' target='_blank'>
          <img src='".$row["path"]."' style='height:200px;width:100%'>
		  </a>
          <div class='caption'>
            <p><a onClick=\"javascript: return confirm('Please confirm deletion');\" href='imgdel.php?id=".$row["ID"]."'>Delete</a></p>
          </div>
        
      </div>
    </div>";
	}
}
    ?>
    </div>
    </div>
    
  </div>
</div>

<footer class="container-fluid text-center">
  <p>© 2017 GAUHATI UNIVERSITY INSTITUTE OF SCIENCE AND TECHNOLOGY ALL RIGHTS RESERVED</p>
</footer>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>
