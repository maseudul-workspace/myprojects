
<?php

$name=$rollno=$pwd1=$pwd2=$email=$branch=$sem="";
$ername=$errollno=$eremail=$erbranch=$ersem=$erpwd1=$erpwd2="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$flag=0;
   if (empty($_POST["name"])) {
     $ername = "Name is required";
	 $flag=1;
   } else {
     $name = test_input($_POST["name"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
		 $flag=1;
       $ername = "Only letters and white space allowed"; 
     }
   }
   
   if (empty($_POST["rollno"])) {
     $errollno = "Roll no is required";
	 $flag=1;
   } else {
     $rollno = test_input($_POST["rollno"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[0-9]*$/",$rollno)) {
       $errollno = "Only numbers are allowed";
	   $flag=1; 
     }
   }
   
   if (empty($_POST["email"])) {
     $eremail = "Email is required";
	 $flag=1;
   } else {
     $email = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $eremail = "Invalid email format";
	   $flag=1; 
     }
   }
	 if (empty($_POST["branch"])) {
     $erbranch = "branch is required";
	 $flag=1;
   } else {
     $branch = test_input($_POST["branch"]);
     // check if branch is well-formed
   }
   if (empty($_POST["Semester"])) {
     $ersem = "semester is required";
	 $flag=1;
   } else {
     $sem = test_input($_POST["Semester"]);
     // check if semeter is well-formed
 
   
   
   
   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "report";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT email FROM registration";
$result = $conn->query($sql);
   
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if(strcmp($row["email"],$email)==0)
		{
			$flag=1;
			$eremail="email already exist";
			break;
		}
    }
}
$conn->close();
   }
   
   
   
   if (empty($_POST["pswd"])) {
     $erpwd1 = "Password is required";
	 $flag=1;
   } else {
     $pwd1 = test_input($_POST["pswd"]);
   }
   
   if (empty($_POST["rpswd"])) {
     $erpwd2 = "Retyping of password is required";
	 $flag=1;
   } else {
     $pwd2 = test_input($_POST["rpswd"]);
   }
   
   if(strcmp($pwd1,$pwd2)!=0)
   {
	   $erpwd2="Mismatch of password";
	   $flag=1;
   }
   

   
   
   if($flag==0)
   {
	   $servername = "localhost";
$username = "root";
$password = "password";
$dbname = "report";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind

$ver="no";
$stmt = $conn->prepare("INSERT INTO registration(name,rollno,branch,semester,email,password,verify) VALUES (?, ?, ?,?,?,?,?)");
$stmt->bind_param("sisisss",$name,$rollno,$branch,$sem,$email,$pwd1,$ver);
$stmt->execute();
	   
	   $stmt->close();
	  
	   
   echo "Successfull and wait for verification.Check your email to know.";
   

 $conn->close();  
   }
   
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap.min.css">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body style="background-color: #f1f1f1;">

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="newlogin.php">Project Manager</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Registration</a></li>
      </ul>
      
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left" style="background-color:white;"> 
      <form id="form1" name="form1" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  enctype="multipart/form-data">
                      <h2>	Name:</h2>
                        <input type="text" name="name" id="name" style="width:100%;"/><span class="error" >*<?php echo $ername ?></span>
                      
  <h2>  Roll No: </h2>
                      <p>
                   <input type="text" name="rollno" id="name2" style="width:100%;"/>
                   <span class="error" >*<?php echo $errollno ?></p></span>
                      <p>
                        <label for="branch"><h2>branch</h2></label>
                        <select name="branch" id="branch" style="width:100%;">
                          <option value="IT">Information Technology</option>
                          <option value="CSE">Computer Science</option>
                          <option value="ECE">Electronics and Communication</option>
                          <option value="Bio Tech">Bio Technology</option>
                        </select>
                        <span class="error" >*<?php echo $erbranch ?></p></span>
                      </p>
                      <p>
                        <label for="Semester"><h2>Semester</h2></label>
                        <select name="Semester" id="Semester" style="width:100%;">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                        <span class="error" >*<?php echo $ersem ?></p></span>
                      </p>
  <h2>E mail: </h2>
                        <label for="email"></label>
                        <input type="text" name="email" id="email" style="width:100%;"/>
                 <span class="error" >*<?php echo $eremail ?></span>
                
                      </p>
                      <p>
                        <label for="uname"></label>
                      </p>
                      <p>Password:
                        <label for="pswd"></label>
                        <input type="password" name="pswd" maxlength=10 style="width:100%;"/>
                        <span class="error" >*<?php echo $erpwd1 ;?></span>
                      </p>
                      <p>Re-Type Password:
                        <label for="rpswd"></label>
                        <input type="password" name="rpswd"  maxlength=10 style="width:100%;"/>
                        <span class="error" >*<?php echo $erpwd2; ?></span>
                      </p>
                      <input type="submit" value="submit"/>
</form>

    </div>
    
    <div class="col-sm-2 sidenav">
      
    </div>
      </div>
</div>

<footer class="container-fluid text-center">
  <p>© 2017 GAUHATI UNIVERSITY INSTITUTE OF SCIENCE AND TECHNOLOGY ALL RIGHTS RESERVED</p>
</footer>

</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>
