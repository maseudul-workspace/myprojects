create database books;
use books;
create table books (ID int NOT NULL AUTO_INCREMENT primary key,title varchar(100),price int,rent_price int, isbn_no int, description text, author varchar(100),pages int, quantity int, language varchar(50), publisher varchar(100), available int default 1);
desc books;
create table notify (n_id int NOT NULL AUTO_INCREMENT primary key,email varchar(50),book_id int);

create table cart (c_id  int NOT NULL AUTO_INCREMENT primary key,cuemail varchar(50),cuename varchar(50), book_id int);
create table users (u_id int NOT NULL AUTO_INCREMENT primary key,userName varchar(100),Phone int,userPass varchar(20),email varchar(50));

create table address(a_id int NOT NULL AUTO_INCREMENT primary key,email varchar(100),district varchar(50),city varchar(50),village varchar(50),landmark varchar(50),phone_no1 int,phone_no2 int);