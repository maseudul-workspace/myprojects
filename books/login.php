<?php
session_start();
if(isset($_GET["prev"]))
{
$_SESSION["prev"]=$_GET["prev"];
}
if(isset($_GET["book_id"]))
{
$_SESSION["book_id"]=$_GET["book_id"];

}

include 'connection.php' ;
$error=$emailError=$passError=$wholeror="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {	
		
		// prevent sql injections/ clear user invalid inputs
		$email = trim($_POST['email']);
		$email = strip_tags($email);
		$email = htmlspecialchars($email);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		// prevent sql injections / clear user invalid inputs
		
		if(empty($email)){
			$error = true;
			$emailError = "Please enter your email address.";
		} else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$emailError = "Please enter valid email address.";
		}
		
		if(empty($pass)){
			$error = true;
			$passError = "Please enter your password.";
		}
		
		// if there's no error, continue to login
		if (!$error) {
			
			//$password = hash('sha256', $pass); // password hashing using SHA256
		
			$sql="SELECT * FROM users WHERE email='".$email."'";
			$result = $conn->query($sql);
   
			if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
			
			if($row['userPass']==$pass ) {
				$_SESSION['user'] = $row['userId'];
				$_SESSION['email']=$row['email'];
				$_SESSION['uname']=$row['userName'];
				if($_SESSION["prev"]==1)
				{
					header("Location:index1.php");
  exit();
  

				}
				else if($_SESSION["prev"]==2)
				{
					header("Location:try.php?book_id=".$_SESSION["book_id"]);
 exit();
 
				}
				else
				{
					header("Location:index.php");
  exit();
				}
				
			}
			else
			{
				$wholeror="Password doesnot match";
			}
	}
			}
			else
			{
				$wholeror="Email doesnot exist";
			}
		
		}}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets\css\bootstrap.min.css">
  <script src="assets\jquery\jquery.min.js"></script>
  <script src="assets\js\bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
	.error{
		color:red;
	}
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Login</a></li>
      </ul>
      
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left"> 
      <h2>Login</h2>
  <form  method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" placeholder="Enter Your Email" name="email">
      <span class="error"><?php echo $emailError; ?></span>
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter Your password" name="pass">
      <span class="error"><?php echo $passError; ?></span>
    </div>
    
    <span class="error"><?php echo $wholeror; ?></span>
    <a href="forgt.php" ><h5 style="text-align:right;">Forget Password</h5></a>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
  <h5>Not have account???<a href="rgstr.php?prev=<?php echo $_SESSION["prev"]; ?>">Register</a></h5>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>



</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>