<?php

session_start();

include 'connection.php' ;

$nameError=$name=$phone1=$phoneError1=$phone2=$phoneError2=$dist=$diserror=$vill=$villerror=$city=$cityerror=$land=$landerror=$pin=$pinerror="";
$select1=NULL;
$select2="Select your district";

$sql="select * from address where email='".$_SESSION["email"]."'";

$error=0;
$flag=0;
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	$flag=1;
    // output data of each row
    while($row = $result->fetch_assoc()) {
		$name=$row["name"];
		$phone1=$row["phone_no1"];
		$phone2=$row["phone_no2"];
		$dist=$row["district"];
		$city=$row["city"];
		$vill=$row["village"];
		$land=$row["landmark"];
		$pin=$row["pin"];
		$select1=$select2=$dist;
	}
}

if ( isset($_POST['submit']) ) {
	
	if(empty($_POST["name"]))
	{
		$nameError="Name is required";
		$error=1;
	}
	else
	{
		$name=test_input($_POST["name"]);
		 if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameError = "Only letters and white space allowed"; 
	  $error=1;
    	}
	}
	
	if(empty($_POST["phone1"]))
	{
		$phoneError1="Name is required";
		$error=1;
	}
	else
	{
		$phone1=test_input($_POST["phone1"]);
		 if (!preg_match("/^[0-9]*$/",$phone1)) {
      $phoneError1 = "Only numbers allowed"; 
	  $error=1;
    	}
	}
	
	$phone2=test_input($_POST["phone2"]);
		 if (!preg_match("/^[0-9]*$/",$phone2)) {
      $phoneError2 = "Only numbers allowed"; 
	  $error=1;
    	}
	
	if(empty($_POST["city"]))
	{
		$cityerror="City name is required";
		$error=1;
	}
	else
	{
		$city=test_input($_POST["city"]);
	}
	if(empty($_POST["village"]))
	{
		$villerror="Village name is required";
		$error=1;
	}
	else
	{
		$vill=test_input($_POST["village"]);
	}
	if(empty($_POST["district"]))
	{
		$diserror="Please select a district";
		$error=1;
	}
	else
	{
		$dist=test_input($_POST["district"]);
	}
	$land=test_input($_POST["land"]);
	
	if(empty($_POST["pin"]))
	{
		$pinerror="Pin is required";
		$error=1;
	}
	else
	{
		$pin=test_input($_POST["pin"]);
		 if (!preg_match("/^[0-9]*$/",$pin)) {
      $pinerror = "Only numbers allowed"; 
	  $error=1;
    	}
	}
	
	if($flag==0)
	{
		if($error==0)
		{
		$stmt=$conn->prepare("insert into address (name,email,district,city,village,landmark,phone_no1,phone_no2,pin) values(?,?,?,?,?,?,?,?,?)");
		$stmt->bind_param("ssssssiii",$name,$_SESSION["email"],$dist,$city,$vill,$land,$phone1,$phone2,$pin);
		$stmt->execute();
		header("Location:confirm.html");
  		exit();
		}
	}
	else
	{
		$sql1="update address set name='".$name."',district='".$dist."',city='".$city."',village='".$vill."',landmark='".$land."',phone_no1=".$phone1.					              ",phone_no2=".$phone2.",pin=".$pin." where email='".$_SESSION["email"]."'";
		if ($conn->query($sql1) === TRUE) {
    		echo "Record updated successfully";
			header("Location:confirm.html");
  			exit();
		} else {
    		echo "Error updating record: " . $conn->error;
		}
	}
}
	function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
	



?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Admin Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="assets\css\bootstrap.min.css">
  <script src="assets\jquery\jquery.min.js"></script>
  <script src="assets\js\bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
	
	.text-danger {
		color:red
	}
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Login</a></li>
      </ul>
      
    </div>
  </div>
</nav>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left"> 
      <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
    
    	
        
        	<div class="form-group">
            	<h2 class="">Your address</h2>
            </div>
        
        	<div class="form-group">
            	<hr />
            </div>
            
            
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            	<input type="text" name="name" class="form-control" placeholder="Enter Name" maxlength="50" value="<?php echo $name ?>" />
                </div>
                <span class="text-danger"><?php echo $nameError; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
            	<input type="text" name="phone1" class="form-control" placeholder="Enter Your Primary Phone Number" maxlength="50" value="<?php echo $phone1 ?>" />
                </div>
                <span class="text-danger"><?php echo $phoneError1; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
            	<input type="text" name="phone2" class="form-control" placeholder="Enter Your Secondary Phone Number" maxlength="50" value="<?php echo $phone2 ?>" />
                </div>
                <span class="text-danger"><?php echo $phoneError2; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
                <select name="district" style="width:100%;padding: 8px 15px;border-radius: 2px;">
                					 <option value="<?php echo $select1; ?>"><?php echo $select2; ?></option>
                					 <option value="Barpeta">Barpeta</option>
                                     <option value="Kamrup">Kamrup</option>
                                     <option value="Nalbari">Nalbari</option>
                                     <option value="Nagaon">Nagaon</option>
                                     <option value="Mangaldoi">Mangaldai</option>
                                     <option value="Lakhimpur">Lakhimpur</option>
                                     <option value="Sibsagar">Sibsagar</option>
                                     <option value="Golaghat">Golaghat</option>
                                     <option value="Goalpara">Goalpara</option>
                                     <option value="Kokrajhar">Kokrajhar</option>
                                     <option value="Tezpur">Tezpur</option>
                                     <option value="Jorhat">Jorhat</option>
                                     <option value="Kaziranga">Kaziranga</option>
                                     <option value="Bongaigaon">Bongaigaon</option>
                </select>
            	                </div>
                <span class="text-danger"><?php echo $diserror; ?></span>
            </div>
            
             <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
            	<input type="text" name="city" class="form-control" placeholder="City/Town" value="<?php echo $city ?>" />
                </div>
                <span class="text-danger"><?php echo $cityerror; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
            	<input type="text" name="village" class="form-control" placeholder="Village" value="<?php echo $vill ?>" />
                </div>
                <span class="text-danger"><?php echo $villerror; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
            	<input type="text" name="land" class="form-control" placeholder="Landmark" value="<?php echo $land ?>" />
                </div>
                
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span></span>
            	<input type="text" name="pin" class="form-control" placeholder="Pin Number" maxlength="50" value="<?php echo $pin ?>" />
                </div>
                <span class="text-danger"><?php echo $pinerror; ?></span>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
            	<button type="submit" class="btn btn-block btn-primary" name="submit">Submit</button>
            </div>
            
            
        
        
   
    </form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>


</body>

<!-- Mirrored from www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 27 Nov 2016 18:08:53 GMT -->
</html>